#!/usr/bin/env bash
cd base
for f in *
do
  docker build -f $f -t devbase/$f ../context
done

cd ../custom
for f in *
do
  docker build -f $f -t dev/$f ../context
done
